DELIMITER //
CREATE PROCEDURE get_path_picture_profile(
    IN usern VARCHAR(100)
)
BEGIN
	SELECT profile_picture
	FROM snetwork.User
	WHERE username = usern;
END //

DELIMITER ;