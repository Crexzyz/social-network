CREATE TABLE User (
	username NVARCHAR(30) PRIMARY KEY UNIQUE NOT NULL,
	email NVARCHAR(254) UNIQUE NOT NULL,
	password NVARCHAR (70) NOT NULL,
	birth_date DATE DEFAULT '1940-01-01',
	profile_picture NVARCHAR(100),
	interests NVARCHAR(250),
	skills NVARCHAR (250),
	token_verification NVARCHAR(100),
	verification TINYINT(1)
);
