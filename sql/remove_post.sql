DELIMITER //
CREATE PROCEDURE remove_post(
		IN postid VARCHAR(45),
        IN user VARCHAR(30)
)
BEGIN
    IF EXISTS(SELECT * FROM Post WHERE pid = postid) THEN    
        DELETE FROM Post
        WHERE pid = postid AND (sender = user OR receiver = user);
    END IF;
END //

DELIMITER ;
