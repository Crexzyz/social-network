DELIMITER //
CREATE PROCEDURE new_session(
		IN username VARCHAR(30),
		IN token VARCHAR(20)
)
BEGIN
	INSERT INTO snetwork.User_session 
	VALUES (token,
	 		username, 
	 		ADDTIME(CURRENT_TIMESTAMP, "1:00:00"));
END //

DELIMITER ;
