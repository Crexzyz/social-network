CREATE TABLE Friend (
	username1 NVARCHAR(30) NOT NULL,
	username2 NVARCHAR(30) NOT NULL,

	CONSTRAINT u1_u2_CK CHECK (username1 <> username2),
	CONSTRAINT Friend_1_FK FOREIGN KEY (username1) REFERENCES User(username)
		ON DELETE CASCADE,
	CONSTRAINT Friend_2_FK FOREIGN KEY (username2) REFERENCES User(username)
		ON DELETE CASCADE,

	CONSTRAINT Friend_PK PRIMARY KEY(username1, username2)
);
