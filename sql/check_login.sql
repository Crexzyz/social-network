DELIMITER //

CREATE PROCEDURE check_login(
        IN usern VARCHAR(30),
        IN pass VARCHAR(70)
)
BEGIN
	DECLARE spass VARCHAR(70) DEFAULT "";
	IF EXISTS(SELECT * FROM snetwork.User WHERE username=usern) THEN
		SET @spass = (SELECT password
    				  FROM snetwork.User
	    			  WHERE username = usern);

		SELECT IF(STRCMP(@spass, pass) = 0, 0, 1) AS state;
	ELSE
		SELECT 2 AS state;
    END IF;
END //

DELIMITER ;
