DELIMITER //
CREATE PROCEDURE search_user(
		IN usern VARCHAR(30)
)
BEGIN
	SELECT username, profile_picture FROM snetwork.User WHERE username LIKE CONCAT('%', usern, '%');
END //

DELIMITER ;
