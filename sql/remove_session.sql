DELIMITER //
CREATE PROCEDURE remove_session(
		IN user VARCHAR(30)
)
BEGIN
	DELETE FROM snetwork.User_session
	WHERE username = user;
END //

DELIMITER ;
