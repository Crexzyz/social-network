DELIMITER //
CREATE PROCEDURE get_friends(
		IN user1 VARCHAR(30)
)
BEGIN
	SELECT f.username2, u.profile_picture
	FROM snetwork.Friend f JOIN snetwork.User u on f.username2 = u.username
	WHERE f.username1 = user1;
END //

DELIMITER ;