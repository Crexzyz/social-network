DELIMITER //
CREATE PROCEDURE check_token(
        IN token VARCHAR(100)
)
BEGIN
	IF EXISTS( SELECT token_verification FROM snetwork.User WHERE token_verification=token) THEN
			UPDATE snetwork.User
			SET verification=1
			WHERE token_verification=token;
            SELECT 100 AS state;
	ELSE
		SELECT 101 AS state;
    END IF;
END //

DELIMITER ;