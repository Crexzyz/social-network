DELIMITER //
CREATE PROCEDURE get_username(
		IN tok VARCHAR(20)
)
BEGIN
	SELECT username
	FROM snetwork.User_session 
	WHERE token = tok;
END //

DELIMITER ;