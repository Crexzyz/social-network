DELIMITER //

CREATE PROCEDURE update_user_info(
        IN olduser NVARCHAR(30),
        IN newuser NVARCHAR(30),
        IN mail NVARCHAR(254),
        IN pass NVARCHAR(70),
        IN b_day INT,
        IN b_month INT,
		IN p_pic NVARCHAR(100),
		IN interst NVARCHAR(250),
		IN skls NVARCHAR (250),
        IN new_token NVARCHAR(100)
)
BEGIN
    DECLARE bd DATE DEFAULT '1940-01-01';
    DECLARE old_mail NVARCHAR(254);
	IF EXISTS(SELECT * FROM snetwork.User WHERE username=olduser) THEN
		UPDATE snetwork.User
        SET password = pass
        WHERE pass IS NOT NULL AND username = olduser; 

        SET @old_mail = (SELECT email FROM snetwork.User WHERE username = olduser);

        IF mail IS NOT NULL AND @old_mail <> mail THEN
            UPDATE snetwork.User
            SET email = mail, token_verification = new_token, verification = 0
            WHERE mail IS NOT NULL AND username = olduser;
        END IF;

		UPDATE snetwork.User
        SET profile_picture = p_pic
        WHERE p_pic IS NOT NULL AND username = olduser; 

        UPDATE snetwork.User
        SET interests = interst
        WHERE interst IS NOT NULL AND username = olduser; 

		UPDATE snetwork.User
        SET skills = skls
        WHERE skls IS NOT NULL AND username = olduser; 
        
        SET bd = (SELECT birth_date FROM User WHERE username = olduser);

        UPDATE snetwork.User
        SET birth_date = STR_TO_DATE(CONCAT('1940-', b_month, '-', b_day), '%Y-%m-%d')
        WHERE b_day IS NOT NULL AND b_month IS NOT NULL AND username = olduser; 

        UPDATE snetwork.User
        SET birth_date = STR_TO_DATE(CONCAT('1940-', MONTH(bd), '-', b_day), '%Y-%m-%d')
        WHERE b_day IS NOT NULL AND b_month IS NULL AND username = olduser; 

        UPDATE snetwork.User
        SET birth_date = STR_TO_DATE(CONCAT('1940-', b_month, '-', DAY(bd)), '%Y-%m-%d')
        WHERE b_month IS NOT NULL AND b_day IS NULL AND username = olduser; 

        UPDATE snetwork.User
        SET username = newuser
        WHERE newuser IS NOT NULL AND username = olduser; 
	ELSE
		SELECT 'no_user' AS state;
    END IF;
END //

DELIMITER ;