DELIMITER //
CREATE PROCEDURE get_post_img(
		IN postid VARCHAR(45)
)
BEGIN
	SELECT picture
	FROM snetwork.Post
	WHERE pid = postid;
END //

DELIMITER ;