DELIMITER //

CREATE PROCEDURE get_user_info(
    IN usern VARCHAR(30)
)
BEGIN
    IF EXISTS(SELECT * FROM snetwork.User WHERE username=usern) THEN
        SELECT username, email, DAY(birth_date), MONTH(birth_date), 
            interests, skills, profile_picture
        FROM snetwork.User
        WHERE username = usern;
    ELSE
        SELECT 1 AS state;
    END IF;
END //

DELIMITER ;
