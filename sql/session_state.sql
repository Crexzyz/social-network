DELIMITER //
CREATE PROCEDURE session_state(
		IN suser VARCHAR(30),
		IN stokn VARCHAR(20)
)
BEGIN
	DECLARE session_date DATETIME DEFAULT CURRENT_DATE;
	DECLARE verif_state INT DEFAULT 0;
	IF EXISTS(SELECT * FROM snetwork.User_session WHERE username = suser) THEN
		IF EXISTS(SELECT * FROM snetwork.User_session WHERE token = stokn) THEN
			SET @verif_state = (SELECT verification FROM snetwork.User WHERE username = suser);

			IF @verif_state = 0 THEN
				SELECT 5 AS state;
			ELSE
				SET @session_state = (SELECT expire_date
									FROM snetwork.User_session
									WHERE username=suser AND token = stokn);
				

				-- 0: session ok, 1: expired session
				SELECT IF (CURRENT_TIMESTAMP < @session_state, 0, 1) AS state;
			END IF;
		ELSE
			-- Bad token
			SELECT 2 AS state;
		END IF;
	ELSE
		-- Session does not exist
		SELECT 3 as state;
	END IF;
END //

DELIMITER ;
