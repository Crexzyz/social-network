DELIMITER //
CREATE PROCEDURE get_username(
    IN usern VARCHAR(30)
)
BEGIN
	SELECT token_verification
    FROM snetwork.User
    WHERE username = usern;

END //

DELIMITER ;