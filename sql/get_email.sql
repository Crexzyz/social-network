DELIMITER //
CREATE PROCEDURE get_email(
    IN usern VARCHAR(30)
)
BEGIN
	SELECT email
    FROM snetwork.User
    WHERE username = usern;

END //

DELIMITER ;