DELIMITER //

CREATE PROCEDURE get_posts(
        IN usern VARCHAR(30)
)
BEGIN
	IF EXISTS(SELECT * FROM snetwork.User WHERE username=usern) THEN
		SELECT * 
		FROM snetwork.Post
		WHERE sender = usern OR receiver = usern
		ORDER BY date DESC;
	ELSE
		SELECT 2 AS state;
    END IF;
END //

DELIMITER ;
