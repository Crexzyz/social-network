DELIMITER //
CREATE PROCEDURE add_post(
		IN sender VARCHAR(30),
		IN receiver VARCHAR(30),
		IN message NVARCHAR(256),
		IN picture NVARCHAR(100),
		IN pid NVARCHAR(45)
)
BEGIN
	INSERT INTO Post
	VALUES (sender, receiver, message, CURRENT_TIMESTAMP, picture, pid);
END //

DELIMITER ;
