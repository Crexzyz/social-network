DELIMITER //
CREATE PROCEDURE add_friend(
		IN user1 VARCHAR(30),
		IN user2 VARCHAR(30)
)
BEGIN
	INSERT INTO snetwork.Friend
	VALUES(user1, user2);
END //

DELIMITER ;