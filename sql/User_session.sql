CREATE TABLE User_session (
	token NVARCHAR(20) NOT NULL PRIMARY KEY,
	username NVARCHAR(30) NOT NULL UNIQUE,
	expire_date DATETIME NOT NULL,

	CONSTRAINT Session_user_FK FOREIGN KEY (username) REFERENCES User(username)
		ON DELETE CASCADE ON UPDATE CASCADE
);
