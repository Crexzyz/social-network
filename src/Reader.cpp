#include "social-network/Reader.h"
#include "social-network/Utils.h"

#include <iostream>
#include <cstring>
#include <cstdlib>

Reader::Reader() 
{
    this->read_elements();
}

Reader::~Reader() {}


void Reader::read_elements()
{
    this->read_cookies();

    std::string first_line;
    std::getline(std::cin, first_line);

    char * params = std::getenv("QUERY_STRING");
    if(params)
        this->read_plain(std::string(params), this->elements);
    
    if(first_line.compare(0, 24, "------WebKitFormBoundary") == 0 ||
        first_line.compare(0, 2, "--") == 0)
        this->read_multipart(first_line);
    else
        this->read_plain(first_line, this->elements);

}

void Reader::read_cookies()
{
    char * cookies = std::getenv("HTTP_COOKIE");
    if(cookies)
        this->read_plain(std::string(cookies), this->cookies);
}

void Reader::read_multipart(const std::string & separator)
{
    long size = atol(std::getenv("CONTENT_LENGTH"));
    long current_size = separator.size();

    int read_state = NEW_ELEMENT;

    std::string current_line = "";
    std::string prev_line = "";

    std::string element;
    std::string value;

    while (current_size < size && !std::cin.eof())
    {
        std::getline(std::cin, current_line);

        if(current_line.compare(0, 24, separator) == 0)
        {
            element = value = "";
            read_state = NEW_ELEMENT;
        }
        else if( read_state == NEW_ELEMENT && current_line.compare(0, 38, "Content-Disposition: form-data; name=\"") == 0 )
        {
            element = find_element_name(current_line, "name=\"");
            read_state = VALUE_READ_NEWLINE;
        }
        else if((read_state == VALUE_READ_NEWLINE || read_state == IMAGE_READ_NEWLINE) && current_line.size() == 1)
        {
            read_state = read_state == VALUE_READ_NEWLINE ? READING_VALUE : READING_IMAGE;
        }
        else if(read_state == VALUE_READ_NEWLINE && 
                (current_line.compare(0, 23,"Content-Type: image/png") == 0 
                || current_line.compare(0, 23, "Content-Type: image/jpg") == 0))
        {
            value = find_element_name(prev_line, "filename=\"");
            this->file_name = value;
            this->elements.insert(std::pair<std::string, std::string>(element, value));
            element = value = "";
            read_state = IMAGE_READ_NEWLINE;
        }
        else if(read_state == READING_VALUE)
        {
            std::cout << "insert " << element << "," << current_line << std::endl;
            this->elements.insert(std::pair<std::string, std::string>(element, current_line));
            element = value = "";
            read_state = NEW_ELEMENT;
        }
        else if(read_state == READING_IMAGE)
        {
            this->file_data += current_line;
        }

        // std::cout << "Line [" << current_line << "]" << current_line.size() << std::endl;
        // std::cout << "State: " << read_state <<std::endl
        // << "Size: " << current_size << "/" << size << std::endl;

        current_size += current_line.size();
        prev_line = current_line;
    }
}

std::string Reader::find_element_name(std::string & line, std::string finder)
{
    const size_t offset = finder.size(); // Offset needed to get to the name starting from pos
    size_t pos = line.find(finder);
    size_t len = 0;

    while(line[pos + offset + len] != '\"')
    	++len;

    return line.substr(pos + offset, len);
}

void Reader::read_plain(std::string input, std::map<std::string, std::string> & target)
{
    size_t start_pos = 0;
    size_t len = 0;
    size_t status = READING_NAME;

    std::string name;
    std::string value;

    for(size_t letter = 0; letter < input.size()+1; ++letter)
    {
        if(input[letter] == '%')
        {
            std::string code = input.substr(letter, 3);
            Utils::log(code + ": " + std::to_string(letter) + " -> " + std::to_string(letter+2));
            char ch = Utils::decode_html(code);
            char good[2] = {ch, 0};
            std::string result = std::string(good);
            input.replace(letter, 3, result);
            Utils::log("Result: " + input);
            ++len;
        }
        else if(status == READING_NAME && input[letter] == '=')
        {
            name = input.substr(start_pos, len);
            status = READING_VALUE_P;
            start_pos = letter+1;
            len = 0;
        }
        else if(status == READING_VALUE_P && (input[letter] == '&' || letter == input.size()))
        {  
            value = len > 0 ? input.substr(start_pos, len) : "";
            target.insert(std::pair<std::string, std::string>(name, value));
            status = READING_NAME;
            name = value = "";
            start_pos = letter+1;
            len = 0;
        }
        else if(status == READING_NAME || status == READING_VALUE_P)
        {
            if(input[letter] == '+') input[letter] = ' ';
            ++len;
        }
    
        // std::cout << input[letter] << ": " << status << std::endl;
    }
}

std::string Reader::get_element(std::string name)
{
    for(auto it = this->elements.begin(); it != elements.end(); ++it)
    {
        if(it->first == name)
            return it->second;
    }

    return "";
}

std::string Reader::get_cookie(const std::string name)
{
    for(auto it = this->cookies.begin(); it != cookies.end(); ++it)
    {
        if(it->first == name)
            return it->second;
    }

    return "";
}

std::string Reader::get_filename()
{
    return this->file_name;
}

std::string Reader::get_data()
{
    return this->file_data;
}