#include "social-network/db_connector.h"
#include "social-network/Utils.h"

#include <iostream>
#include <mysql.h>
#include <string>
#include <sstream>

db_connector::db_connector()
{
	this->connect();
}

db_connector::~db_connector()
{
	this->free_result();
	mysql_close(this->connection);
}

bool db_connector::connect()
{
	try
	{
		this->connection = mysql_init(NULL);
        if (!mysql_real_connect(this->connection,this->host, this->user,this->password,
        						this->db_name, this->port, this->socket, 0))
        {
            std::cerr << mysql_error(this->connection) << std::endl;
            return false;
        }
	}
	catch(char * err)
	{
		std::cout << "MYSQL ERROR: " << err << std::endl;
		return false;
	}

	return true;
}

void db_connector::free_result()
{
	if(!this->last_query_freed)
	{
		mysql_free_result(this->query_result);
		this->query_result = 0;
		this->last_query_freed = true;
	}
}

MYSQL_RES * db_connector::query(const char * query)
{
	this->free_result();
	
    if (mysql_query(this->connection, query))
    {
        std::cerr << mysql_error(this->connection) << std::endl;
        return NULL;
    }

    this->query_result = mysql_use_result(this->connection);

	this->last_query_freed = false;
	return this->query_result;
}

void db_connector::clean_string(std::string & subquery)
{
	for (size_t i = 0; i < subquery.size(); ++i) 
	{
	    if (subquery[i] == ';') {
	        subquery.replace(i, 2, "\\;");
	        ++i;
        }
	}
}

void db_connector::register_insert(std::string & username, std::string & email,
								   std::string & password )
{
	this->free_result();

	this->clean_string(username); this->clean_string(email);
	this->clean_string(password);

	std::ostringstream query;

	query << "INSERT INTO snetwork.User(username, email, password, token_verification, verification)"
		  << "VALUES(\'" << username << "\',\'" << email << "\',\'" << password << "\',\'"
		  << Utils::generate_token(100) << "\',\'" << "0" << "\');"; 


	//std::cout << query.str() << std::endl;
    if (mysql_query(this->connection, query.str().c_str()))
    {
        std::cerr << mysql_error(this->connection) << std::endl;
    }
}