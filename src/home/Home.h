#ifndef HOME_H
#define HOME_H

#include <string>
#include <vector>
#include <map>

class Home
{

private:
	const std::string HTML_PATH = "/home/sssnadm/Desktop/social-network/pages/home/home.html";
 	const std::string CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/home/home.css\">";
 	std::vector<std::map<std::string, std::string>> get_posts(std::string username);
public:
	Home();
	~Home();
	int run();
	
};


#endif