#include "Home.h"
#include "social-network/Header.h"
#include "social-network/Authenticator.h"
#include "social-network/db_connector.h"
#include "social-network/Utils.h"
#include "social-network/Reader.h"

#include <iostream>
#include <sstream>
#include <map>
#include <string>
#include <cgicc/Cgicc.h>

Home::Home()
{

}

Home::~Home()
{

}

int Home::run()
{
	Header header;
	Authenticator auth;
	Reader cgi;

	std::string token = auth.token_check(cgi);
	std::string name = auth.get_username(token);
	long session = auth.check_session(name, token);
	
	if(session != Authenticator::VALID_SESSION)
	{
		header.login_redirect(session);
	}
	else
	{
		header.set_logged_in(true);
		header.set_name(name);
		header.print(this->CSS_TAG);

		std::map<std::string, std::string> test_words;
		test_words.insert(std::pair<std::string, std::string>("!${sender}", "Undefined sender"));
		test_words.insert(std::pair<std::string, std::string>("!${receiver}","Undefined receiver"));
		test_words.insert(std::pair<std::string, std::string>("!${message-text}", "No message"));


		std::vector<std::map<std::string, std::string>> posts = this->get_posts(name);
		header.Printer::special_print(test_words, this->HTML_PATH, &posts);
		std::cout << "</body></html>\n";	
	}

	return 0;
}

std::vector<std::map<std::string, std::string>> Home::get_posts(std::string username)
{
	std::vector<std::map<std::string, std::string>> posts;

	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.get_posts(\'" << username << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
		MYSQL_ROW row;
	    while ((row = mysql_fetch_row(result)) != NULL && mysql_num_fields(result) == 6)
	    {
	    	// std::cout << row[2] << std::endl;
			std::map<std::string, std::string> words;
			words.insert(std::pair<std::string, std::string>("!${sender}", row[0]));
			words.insert(std::pair<std::string, std::string>("!${receiver}",row[1]));
			words.insert(std::pair<std::string, std::string>("!${message-text}", row[2]));
			words.insert(std::pair<std::string, std::string>("!${date}", row[3]));

			if(row[4] != NULL)
			{
				words.insert(std::pair<std::string, std::string>("!?{image}", "true"));
				words.insert(std::pair<std::string, std::string>("!${image}", row[4]));
			}

			words.insert(std::pair<std::string, std::string>("!${pid}", row[5]));

			posts.push_back(words);
	    }
	}

	return posts;
}