#include "social-network/Printer.h"
#include "social-network/Utils.h"

#include <iostream>
#include <cstring>


Printer::Printer()
{

}

Printer::~Printer()
{

}

void Printer::print(const std::string & path)
{
	std::ifstream file(path);

	std::string line;

	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			std::cout << line << std::endl;
		}

		file.close();	
	}
	else
		std::cout << "Error: could not open file" << std::endl;
}

void Printer::basic_print()
{
	std::cout << "Content-type:text/html;charset=utf-8\n\n"; 
}

void Printer::cookie_header(std::map<std::string, std::string> & cookies)
{
	std::cout << "Content-type:text/html;charset=utf-8" << std::endl;

	for(auto cookie = cookies.begin(); cookie != cookies.end(); ++cookie)
		this->set_cookie(cookie->first, cookie->second);
	
	std::cout << "\n\n"; 
}

void Printer::set_cookie(std::string key, std::string value)
{
	std::cout << "Set-Cookie: " << key << "=" << value << std::endl;
}

void Printer::login_redirect(int error_code)
{
	std::map<std::string, std::string> map;
	// Cookie that deletes current one, (if there's any)
	map.insert(std::pair<std::string, std::string>("token", "none; Max-Age=0; HttpOnly" ));
	this->redirect(Printer::Redirection_code::LOGIN, &map, error_code);
}

void Printer::redirect(int redirection_code, std::map<std::string, std::string> * cookies, int error_code)
{
	if(cookies != NULL)
		this->cookie_header(*cookies);
	else
		this->basic_print();

	std::map<std::string, std::string> special_words;

	if ( error_code == 100)
		special_words.insert(std::pair<std::string, std::string>("!${error-code}", ""));
	else if(error_code != Printer::NO_ERROR )
		special_words.insert(std::pair<std::string, std::string>("!${error-code}", "?eid=" + std::to_string(error_code)));
	else 
		special_words.insert(std::pair<std::string, std::string>("!${error-code}", ""));
		
	switch(redirection_code)
	{
		case LOGIN:
			special_words.insert( std::pair<std::string, std::string>("!${redirect}", "login") );
			this->special_print(special_words, this->REDIRECT_HTML);
			break;
		case HOME:
			special_words.insert( std::pair<std::string, std::string>("!${redirect}", "home") );
			this->special_print(special_words, this->REDIRECT_HTML);	
			break;
		case FRIENDS:
			special_words.insert( std::pair<std::string, std::string>("!${redirect}", "friends") );
			this->special_print(special_words, this->REDIRECT_HTML);	
			break;
		case REGISTER:
			special_words.insert( std::pair<std::string, std::string>("!${redirect}", "register") );
			this->special_print(special_words, this->REDIRECT_HTML);	
			break;
		default:
			this->print(this->ERROR_HTML);
			return;
	}
}

void Printer::replace_special(std::string & line, const std::map<std::string, std::string> & special_words)
{
	bool skip_endl = false;
	if ((skip_endl = (line.compare(0, 3, "!$$") == 0)))
		line.replace(0, 3, "");
	else
		line.replace(0, 2, "");

	for(auto it = special_words.begin(); it != special_words.end(); ++it)
	{
		int start_pos = line.find(it->first);
		if(start_pos != -1)
			line.replace(start_pos, it->first.size(), it->second);
	}

	std::cout << line;
	if(!skip_endl)
		std::cout << std::endl;
}

void Printer::special_print(const std::map<std::string, std::string> & special_words,
					const std::string & special_path,
					const std::vector< std::map<std::string, std::string> > * posts)
{	
	std::ifstream file(special_path);

	std::string line;

	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			if(line.compare(0, 2, "!$") == 0)
			{
				this->replace_special(line, special_words);
				continue;
			}
			else if (line.compare(0, 2, "!#") == 0)
			{				
				line.replace(0, 2, " ");
				if(posts != NULL)
					this->loop_print(special_words, file, *posts);
				continue;
			}
			else if(line.compare(0, 2, "!?") == 0)
			{
				this->conditional_print(line, file, special_words, posts);
				continue;
			}

			std::cout << line << std::endl;
		}

		file.close();	
	}
	else
		std::cout << "Error" << std::endl;
}

void Printer::special_print(const std::map<std::string, std::string> & special_words,
						    const std::string & special_path)
{
	this->special_print(special_words, special_path, NULL);
}

void Printer::loop_print(const std::map<std::string, std::string> & special_words,
						std::ifstream & opened_file, 
						 const std::vector<std::map<std::string, std::string>> & posts)
{
	std::vector<std::string> lines;
	std::string line;
	int loop_end_found = 0;

	while(std::getline(opened_file, line))
	{
		if (line.compare(0, 2, "!#") == 0)
		{
			line.replace(0, 2, " ");
			loop_end_found = 1;
			break;
		}
		lines.push_back(line);
	}

	if(!loop_end_found)
		return;

	for (auto it = posts.begin(); it != posts.end(); ++it)
	{
		for(auto current_line = lines.begin(); current_line != lines.end(); ++current_line)
		{
			std::string templine = std::string(*current_line);
			if(templine.compare(0, 2, "!$") == 0)
			{
				templine.replace(0, 2, " ");
				this->replace_special(templine, *it);
				continue;
			}
			else if(templine.compare(0, 2, "!?") == 0)
			{
				this->conditional_print(special_words, current_line, *it);
				continue;
			}
			else
			{
				std::cout << templine << std::endl;
			}
		}
	}
}


bool Printer::check_conditional_print(std::string variable,
									 const std::map<std::string, std::string> & special_words,
									 const std::map<std::string, std::string> & alternate_words)
{
	bool print_data = false;

	// Find if variable declared in special_words
	for(auto it = special_words.begin(); it != special_words.end(); ++it)
	{
		bool print = (variable == it->first && it->second == "true");
		if(print)
			print_data = true;
	}
	// Else, find in alternate map (posts map)
	if(!print_data)
	{
		for(auto it = alternate_words.begin(); it != alternate_words.end(); ++it)
		{
			bool print = (variable == it->first && it->second == "true");
			if(print)
				print_data = true;
		}
	}

	return print_data;
}

void Printer::conditional_print(const std::map<std::string, std::string> & variables,
								std::vector<std::string>::iterator & line_iterator,
 								const std::map<std::string, std::string> & special_words)
{
	std::string cond_variable = std::string(*line_iterator);

	++line_iterator;

	bool print_data = this->check_conditional_print(cond_variable, variables, special_words);

	std::string line = std::string(*line_iterator);

	int nested_ifs = 0;

	while( !(line.compare(0, 3, "!?[") == 0 && nested_ifs < 0))
	{		
		if(line.compare(0, 3, "!?{") == 0) ++nested_ifs;

		if(print_data)
		{
			if(line.compare(0, 2, "!$") == 0)
			{
				this->replace_special(line, special_words);
			}
			else if(line.compare(0, 2, "!?") == 0)
			{
				this->conditional_print(variables, line_iterator, special_words);
				continue;
			}
			else
			{
				std::cout << line << std::endl;
			}
		}		

		++line_iterator;
		line = std::string(*line_iterator);
		if(line.compare(0, 3, "!?[") == 0) --nested_ifs;
		
	}
}

void Printer::conditional_print(std::string & cond_variable, 
						std::ifstream & opened_file,
						const std::map<std::string, std::string> & special_words,
						const std::vector< std::map<std::string, std::string> > * posts)
{
	bool print_data = this->check_conditional_print(cond_variable, special_words, special_words);

	std::string line;

	int nested_ifs = 0;

	std::getline(opened_file, line);
	while(!(line.compare(0, 3, "!?[") == 0 && nested_ifs < 0))
	{
		if(line.compare(0, 3, "!?{") == 0) ++nested_ifs;

		if(print_data)
		{
			if(line.compare(0, 2, "!$") == 0)
			{
				this->replace_special(line, special_words);
			}
			else if (line.compare(0, 2, "!#") == 0)
			{				
				line.replace(0, 2, " ");
				if(posts != NULL)
					this->loop_print(special_words, opened_file, *posts);
				continue;
			}
			else
			{
				std::cout << line << std::endl;
			}
		}

		std::getline(opened_file, line);
		if(line.compare(0, 3, "!?[") == 0) --nested_ifs;
	}
}

