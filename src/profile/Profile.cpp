#include "Profile.h"

#include "social-network/Header.h"
#include "social-network/Printer.h"
#include "social-network/db_connector.h"
#include "social-network/Image.h"
#include "social-network/Utils.h"

#include <cstdlib>
#include <cstring>
#include <cgicc/Cgicc.h>
#include <exception>
#include <sstream>

Profile::Profile()
{
	this->auth = new Authenticator();
}

Profile::~Profile()
{
	delete this->auth;
}

int Profile::run()
{
	Header header;
	cgicc::Cgicc cgi;
	std::string token = this->auth->token_check(cgi);
	std::string name = this->auth->get_username(token);
	long session = auth->check_session(name, token);

	if(session == Authenticator::VALID_SESSION)
	{
		header.set_logged_in(true);
		header.set_name(name);

		char * env = std::getenv("REQUEST_METHOD");
		if (env != 0 && strncmp(std::getenv("REQUEST_METHOD"), "POST", 4) == 0)
		{
			this->handle_post(cgi, name);
			header.redirect(Printer::Redirection_code::HOME, NULL, Printer::NO_ERROR);
		}
		else
		{
			std::map<std::string, std::string> test_words = this->get_info(name);

			header.print(this->CSS_TAG);
			header.Printer::special_print(test_words, this->HTML_PATH);
			std::cout << "</body></html>\n";
		}
	}
	else
	{
		header.login_redirect(Printer::INVALID_SESSION);
	}

	return 0;
}

std::string Profile::getImageName(std::string username, cgicc::Cgicc & cgi)
{
	db_connector db;

	std::string token = this->auth->token_check(cgi);
	std::ostringstream query;

	query << "CALL snetwork.get_path_picture_profile(\'" << username << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
		MYSQL_ROW row;

	    if ((row = mysql_fetch_row(result)) != NULL)
			return std::string(row[0]);
		else
			return "";
	}
	else
		return "";
}

void Profile::make_validation(std::string name, std::string smail, std::string token)
{
    db_connector db;

    std::ostringstream query;
    query << "CALL snetwork.get_email(\'"
                    << name << "\');";

    MYSQL_RES * result = NULL;
    result = db.query(query.str().c_str());

    if(result != NULL)
	{
        MYSQL_ROW row;

        if ((row = mysql_fetch_row(result)) != NULL)
        {
            std::string old_mail(row[0]);
            if (strcmp(old_mail.c_str(), smail.c_str()) != 0)
                    Utils::resend_mail(name, smail, token);
        }
    }

}

void Profile::handle_post(cgicc::Cgicc & cgi, std::string name)
{
	try
	{
		cgicc::form_iterator email = cgi.getElement("email");
		cgicc::form_iterator password = cgi.getElement("password");
		cgicc::form_iterator skills = cgi.getElement("skills");
		cgicc::form_iterator interests = cgi.getElement("interests");		
		cgicc::form_iterator birthday = cgi.getElement("birthday");
		cgicc::form_iterator birthmonth = cgi.getElement("birthmonth");

		cgicc::const_form_iterator end = cgi.getElements().end();

		// fotito 
		auto file = cgi.getFiles();
		std::string path = "";

		// leer todos los datos
		if(email != end && password != end && skills != end
			&& birthday != end && birthmonth != end)
		{
			std::string smail = std::string((**email).c_str());
			std::string spass = std::string((**password).c_str());
			std::string sskill = std::string((**skills).c_str());
			std::string sint = std::string((**interests).c_str());
			std::string sbirtd = std::string((**birthday).c_str());
			std::string sbirtm = std::string((**birthmonth).c_str());

			if(file.size() > 0 && file.at(0).getFilename().length() > 0 )
			{
				path = Image::save((file.at(0)).getData(), 
				file.at(0).getFilename(), Utils::generate_token(50));				
			}
			else
				path = getImageName(this->auth->get_username(token), cgi);

            std::string new_token = Utils::generate_token(100);
            make_validation(name, smail, new_token);

			db_connector db;

			std::string token = this->auth->token_check(cgi);
			std::ostringstream query;

			std::string real_pass = spass.size() > 7 ? "\'" + spass + "\'" : "null";

			query << "CALL snetwork.update_user_info(\'" 
					<< this->auth->get_username(token) << "\'," 
					<< "null" << ",\'" 
					<< smail << "\'," 
					<< real_pass << ",\'" 
					<< sbirtd << "\',\'" 
					<< sbirtm << "\',\'" 
					<< path << "\',\'" 
					<< sint << "\',\'" 
					<< sskill << "\',\'" 
					<< new_token << "\');";

			Utils::log(query.str());
            Utils::log_audit("Update profile.\tUser: " + name);


			db.query(query.str().c_str());
		}
	}
	catch(std::exception & e)
	{
		std::cout << "F\n";
	}

}


std::map<std::string, std::string> Profile::get_info(std::string username)
{
	std::vector<std::map<std::string, std::string>> posts;

	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.get_user_info(\'" << username << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	std::map<std::string, std::string> words;
	if(result != NULL)
	{
		MYSQL_ROW row;
	    if ((row = mysql_fetch_row(result)) != NULL && mysql_num_fields(result) < 8)
	    {
			if(row[6] != NULL)
				words.insert(std::pair<std::string, std::string>("!${image}", row[6]));

			words.insert(std::pair<std::string, std::string>("!${username}", row[0]));
			words.insert(std::pair<std::string, std::string>("!${email}",row[1]));
			words.insert(std::pair<std::string, std::string>("!${birth-day}", row[2] == NULL ? "" : row[2]));
			words.insert(std::pair<std::string, std::string>("!${birth-month}", row[3]== NULL ? "" : row[3]));
			words.insert(std::pair<std::string, std::string>("!${interests}", row[4]== NULL ? "" : row[4]));
			words.insert(std::pair<std::string, std::string>("!${skills}", row[5]== NULL ? "" : row[5]));

	    }
	}

	return words;
}