#ifndef PROFILE_H
#define PROFILE_H
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "social-network/Authenticator.h"

class Profile
{
private:
	const std::string CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/profile/profile.css\">";
	const std::string HTML_PATH = "/home/sssnadm/Desktop/social-network/pages/profile/profile.html";
	void handle_post(cgicc::Cgicc & cgi, std::string name);
 	std::map<std::string, std::string> get_info(std::string username);
	std::string getImageName(std::string username, cgicc::Cgicc & cgi);
	void make_validation(std::string name, std::string smail, std::string token);
	Authenticator * auth;

public:
	Profile();
	~Profile();
	int run();
};



#endif