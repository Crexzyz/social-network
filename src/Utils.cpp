#include "social-network/Utils.h"
#include "social-network/db_connector.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <stdlib.h> 

std::vector<std::map<std::string, std::string>> Utils::get_friends(std::string & username)
{
	std::vector<std::map<std::string, std::string>> friends;

	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.get_friends(\'" << username << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
		MYSQL_ROW row;
	    while ((row = mysql_fetch_row(result)) != NULL && mysql_num_fields(result) == 1 )
	    {
	    	std::map<std::string, std::string> temp;
			temp.insert(std::pair<std::string, std::string>("!${friend}", row[0]));

			friends.push_back(temp);
	    }
	}

	return friends;

}

std::string Utils::generate_token(unsigned int size)
{
	static const char token_values[] =
	"0123456789"
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz";

	// Use a max of 100 chars
	if(size <= 101 && size > 1)
	{
		char token[size];
		std::memset(token, 0, size);

		srand(time(0)); 
		for (size_t i = 0; i < size-1; ++i) 
			token[i] = token_values[rand() % (sizeof(token_values) - 1)];

    	return std::string(token); 
	}

	return std::string("");
}

void Utils::log(std::string text)
{
	std::ofstream outfile ("/var/www/logs/snet-log.txt", std::ios_base::app);	

	outfile << text << std::endl;

	outfile.close();
}

void Utils::log_audit(std::string text)
{
	auto start = std::chrono::system_clock::now();
	// Some computation here
	auto end = std::chrono::system_clock::now();

	std::chrono::duration<double> elapsed_seconds = end-start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	std::string log(std::ctime(&end_time) + text);

	std::ofstream outfile ("/var/www/logs/snet-log-status.txt", std::ios_base::app);

	outfile <<log << std::endl;

    std::string url = "echo \"Bye, you logged in Super Secure Social Network. ";
    url = url +  " | mail -v -s \"Alert\" \"" + "student42@unac.ucr.ac.cr" + "\"";
    const char *command1 = url.c_str();
    system(command1);


	outfile.close();
}


void Utils::raw_print()
{
	std::cout << "<div class=\"content\"><div class=\"container center shadow main-container\">" << std::endl;
	std::cout << "RAW INPUT (" << std::getenv("CONTENT_LENGTH") << "): " << std::endl;
	for(std::string line; std::getline(std::cin, line);)
	{
		std::cout << line << std::endl;
	}
	std::cout << "</div></div>" << std::endl;
}

void Utils::send_mail(std::string & username, std::string & mail)
{
	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.get_token(\'" << username << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
	   MYSQL_ROW row;

	    if ((row = mysql_fetch_row(result)) != NULL)
	    {
	     	std::string token(row[0]);

	        std::string url = "echo \"Welcome to Super Secure Social Network. Verification url: https://172.24.129.50/cgi-bin/validate?id=\"" + token;
	        url = url +  " | mail -v -s \"Message\" \"" + mail + "\"";


	        const char *command1 = url.c_str();
	        system(command1);
	    }
	}
}


void Utils::resend_mail(std::string & username, std::string & mail, std::string token)
{
    std::string url = "echo \"Welcome to Super Secure Social Network. Verification url: https://172.24.129.50/cgi-bin/validate?id=\"" + token;
    url = url +  " | mail -v -s \"Message\" \"" + mail + "\"";

    const char *command1 = url.c_str();
    system(command1);

}

void Utils::send_notify_email(std::string & username)
{
	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.get_email(\'" << username << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
	   MYSQL_ROW row;

        if ((row = mysql_fetch_row(result)) != NULL)
        {
         	std::string mail(row[0]);

            std::string url = "echo \"Welcome. You logged in Super Secure Social Network.\"";
            url = url +  " | mail -v -s \"Message\" \"" + mail + "\"";

            Utils::log(url);
            const char *command1 = url.c_str();
            system(command1);
        }
	}
}

char Utils::decode_html(std::string & text)
{
	if(text == "%20") return '!';
	else if(text == "%2A") return '*';
	else if(text == "%27") return '\'';
	else if(text == "%28") return '(';
	else if(text == "%29") return ')';
	else if(text == "%3B") return ';';
	else if(text == "%3A") return ':';
	else if(text == "%40") return '@';
	else if(text == "%26") return '&';
	else if(text == "%3D") return '=';
	else if(text == "%2B") return '+';
	else if(text == "%24") return '$';
	else if(text == "%2C") return ',';
	else if(text == "%2F") return '/';
	else if(text == "%3F") return '?';
	else if(text == "%25") return '%';
	else if(text == "%23") return '#';
	else if(text == "%5B") return '[';
	else if(text == "%5D") return ']';
	else return ' ';
}