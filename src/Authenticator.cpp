#include "social-network/Authenticator.h"
#include "social-network/Header.h"
#include "social-network/Printer.h"
#include "social-network/db_connector.h"
#include "social-network/Utils.h"

#include <mysql.h>
#include <sstream>
#include <cstdlib>

Authenticator::Authenticator() {}

Authenticator::~Authenticator() {}

std::string Authenticator::token_check(Reader & reader)
{
	std::string cookie = reader.get_cookie("token");
	if(cookie.size() > 1)
		return cookie;
	else
		return "";
}

std::string Authenticator::token_check(cgicc::Cgicc & cgi)
{
	auto cookies = cgi.getEnvironment().getCookieList();

	for(auto cookie = cookies.begin(); cookie != cookies.end(); ++cookie)
	{
		if((*cookie).getName().compare("token") == 0)
		{
			return (*cookie).getValue();
		}
	}

	return "none";
}

void Authenticator::new_session(std::string username, std::string token)
{
	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.new_session(\'" << username << "\',\'" << token << "\');";

	db.query(query.str().c_str());
}

void Authenticator::remove_session(std::string username)
{
	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.remove_session(\'" << username << "\');";

	db.query(query.str().c_str());
}

long Authenticator::check_session(std::string username, std::string token)
{
	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.session_state(\'" << username << "\',\'" << token << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
		MYSQL_ROW row;

	    if ((row = mysql_fetch_row(result)) != NULL)
			return atol(row[0]);
		else
			return Error_code::EMPTY_RESULT;
	}
	else
		return Error_code::QUERY_ERROR;
}

long Authenticator::check_login(std::string username, std::string password)
{
	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.check_login(\'" << username << "\',\'" << password << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
		MYSQL_ROW row;

	    if ((row = mysql_fetch_row(result)) != NULL)
	    {
			Utils::send_notify(username);
			return atol(row[0]);
	    }
		else
			return Error_code::EMPTY_RESULT;
	}
	else
		return Error_code::QUERY_ERROR;
}

std::string Authenticator::get_username(std::string token)
{
	db_connector db;

	std::ostringstream query;

	query << "CALL get_username(\'" << token << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
		MYSQL_ROW row;

	    if ((row = mysql_fetch_row(result)) != NULL)
	    {
	    	std::string name(row[0]);
			return name;
	    }
		else
			return "";
	}
	else
	{		
		return "";
	}
}