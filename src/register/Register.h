#ifndef LOGIN_H
#define LOGIN_H
#include <string>
#include <iostream>
#include <fstream>

class Register
{
private:
	const std::string CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/register/register.css\">";
	const std::string HTML_PATH = "/home/sssnadm/Desktop/social-network/pages/register/register.html";

	void handle_post();
	bool is_email_valid(const std::string & email);
	bool is_user_valid(const std::string & user);
	bool is_pass_valid(const std::string & pass);
	std::string validate_data(const std::string & user, const std::string & mail, const std::string & pass);

public:
	Register();
	~Register();
	int run();
};



#endif