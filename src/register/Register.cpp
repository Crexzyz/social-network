#include "Register.h"

#include "social-network/Header.h"
#include "social-network/Printer.h"
#include "social-network/db_connector.h"
#include "social-network/Utils.h"
#include "social-network/Reader.h"

#include <cstdlib>
#include <cstring>
#include <cgicc/Cgicc.h>
#include <exception>
#include <regex>

Register::Register(){}

Register::~Register(){}

int Register::run()
{
	char * env = std::getenv("REQUEST_METHOD");
	if (env != 0 && strncmp(std::getenv("REQUEST_METHOD"), "POST", 4) == 0)
	{
		this->handle_post();
	}
	else
	{
		Header header;
		Reader cgi;
		std::map<std::string, std::string> special_words;
		special_words.insert(std::pair<std::string, std::string>("!?{error}", "true"));

		std::string error_id = cgi.get_element("eid");
		std::vector<std::map<std::string, std::string>> errors;
		if(error_id.size() > 0)
		{
			int code = atoi(error_id.c_str());
			int error = 0;
			while(code != 0)
			{
				bool insert = false;
				int data = code % 10;
				std::map<std::string, std::string> words;

				if((insert = (error == 0 && data == 1)))
					words.insert(std::pair<std::string, std::string>("!${error}", "Password length must be 8 characters min. Use ASCII-only characters"));
				else if((insert = (error == 1 && data == 1)))
					words.insert(std::pair<std::string, std::string>("!${error}", "Invalid email address"));
				else if((insert = (error == 2 && data == 1)))
					words.insert(std::pair<std::string, std::string>("!${error}", "Usernames accept letters, numbers, underscores, hyphens, or dots"));

				if(insert)
					errors.push_back(words);

				++error;
				code /= 10;
			}
		}
		header.print(this->CSS_TAG);
		header.Printer::special_print(special_words, this->HTML_PATH, &errors);
		std::cout << "</body></html>\n";
	}

	return 0;

}

void Register::handle_post()
{
	try 
	{
		Reader cgi;
		std::string name = cgi.get_element("username");
		std::string mail = cgi.get_element("email");
		std::string pass = cgi.get_element("password");

		Header header;
		if(name.size() > 0 && mail.size() > 0  && pass.size() > 0 )
		{
			db_connector db;
			std::string sname = std::string(name.c_str());
		 	std::string smail = std::string(mail.c_str());
			std::string spass = std::string(pass.c_str());

			std::string errors = this->validate_data(sname, smail, spass);

			if(errors == "000")
			{
				db.register_insert(sname, smail, spass);
				header.redirect(Printer::Redirection_code::LOGIN, NULL, Printer::NO_ERROR);
			}
			else
			{
				header.redirect(Printer::Redirection_code::REGISTER, NULL, atoi(errors.c_str()));
			}
		}
		else
		{
			char errors[4]; memset(errors, 0, 4);

			errors[0] = name.size() > 0 ? '0' : '1';
			errors[1] = mail.size() > 0 ? '0' : '1';
			errors[2] = pass.size() > 0 ? '0' : '1';
			header.redirect(Printer::Redirection_code::REGISTER, NULL, atoi(errors));
		}

		// std::cout << cgi.getEnvironment().getRemoteAddr() << std::endl;
	}
	catch(std::exception & e)
	{
		std::cout << "F " << e.what();
	}
}

std::string Register::validate_data(const std::string & user, const std::string & mail, const std::string & pass)
{
	char errors[4]; memset(errors, 0, 4);

	errors[0] = is_user_valid(user) ? '0' : '1';
	errors[1] = is_email_valid(mail) ? '0' : '1';
	errors[2] = is_pass_valid(pass) ? '0' : '1';

	return std::string(errors);
}

bool Register::is_user_valid(const std::string & user)
{
	const std::regex pattern("^(\\w+|[\\.\\-])+$");

	return std::regex_match(user, pattern);
}

bool Register::is_email_valid(const std::string & email)
{
	const std::regex pattern("^[\\w\\.\\-]+@[\\w\\.]+\\.[a-zA-Z]{2,5}$");

	return std::regex_match(email, pattern);
}

bool Register::is_pass_valid(const std::string & pass)
{
	// Password chars from https://owasp.org/www-community/password-special-characters
	const std::regex pattern("^(\\w+|[!\"#$%&\'()*+,\\-.\\/:;<=>?@[\\]\\^_`{|}~])+$");

	return pass.size() > 7 && std::regex_match(pass, pattern);
}