#ifndef VALIDATE_H
#define VALIDATE_H
#include <string>
#include <iostream>
#include <fstream>

class Validate
{
private:
	const std::string CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/Validate/Validate.css\">";
	const std::string HTML_PATH = "/home/sssnadm/Desktop/social-network/pages/Validate/Validate.html";
	long check_token(std::string token);

public:
	Validate();
	~Validate();
	int run();
};



#endif