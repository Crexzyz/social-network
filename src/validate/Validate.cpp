#include "Validate.h"

#include "social-network/Header.h"
#include "social-network/Printer.h"
#include "social-network/db_connector.h"
#include "social-network/Utils.h"
#include "social-network/Reader.h"

#include <cstdlib>
#include <cstring>
#include <cgicc/Cgicc.h>
#include <exception>
#include <regex>

Validate::Validate(){}

Validate::~Validate(){}

int Validate::run()
{
	Header header;
	Reader cgi;
	// char * env = std::getenv("REQUEST_METHOD");
	// // If server gets a GET
	// if (env != 0 && strncmp(env, "GET", 3) == 0)
	std::string token = cgi.get_element("id");
	Utils::log(token);

	if (token.size() > 0)
	{
		long code_check = check_token(token);

		//header.redirect(Printer::Redirection_code::HOME, NULL, Printer::NO_ERROR);
		header.login_redirect(code_check);
	}
	else
	{
		header.login_redirect(27);
	}
	
	
	return 0;
}

long Validate::check_token(std::string token)
{
	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.check_token(\'" << token << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
		MYSQL_ROW row;

	    if ((row = mysql_fetch_row(result)) != NULL)
			return atol(row[0]);
		else
			return 101;
	}
	else
		return 101;
}

