#include "Login.h"

#include "social-network/Printer.h"
#include "social-network/db_connector.h"
#include "social-network/Utils.h"
#include "social-network/Reader.h"

#include <map>
#include <exception>
#include <cstdlib>
#include <cstring>

Login::Login()
{
	this->auth = new Authenticator();
}

Login::~Login()
{
	delete this->auth;
}

int Login::run()
{
	Reader cgi; 
	Header header;
	std::string token = this->auth->token_check(cgi);
	std::string user = this->auth->get_username(token); // Note this user is "" if token is "none"
	long session = this->auth->check_session(user, token);

	char * env = std::getenv("REQUEST_METHOD");
	// If server gets a POST
	if (env != 0 && strncmp(env, "POST", 4) == 0)
	{
		this->handle_post(header, cgi);			
	}
	else
	{
		// If the user has a valid cookie, redirect to home
		if(session == Authenticator::VALID_SESSION)
		{
			header.redirect(Printer::Redirection_code::HOME, NULL, Authenticator::VALID_SESSION);
		}
		else // Print login page
		{
			std::map<std::string, std::string> special_words;

			std::string error_id = cgi.get_element("eid");
			if(error_id.size() > 0)
			{
				int code = atoi(error_id.c_str());
				if(code != Printer::NO_ERROR)
				{
					special_words.insert(std::pair<std::string, std::string>("!?{error}", "true"));
					special_words.insert(std::pair<std::string, std::string>("!${error-string}", this->error_to_string(code)));
				}
			}

			header.print(this->CSS_TAG);
			header.Printer::special_print(special_words, this->HTML_PATH);
			std::cout << "</body></html>\n";
		}
	}
	
	return 0;
}

void Login::handle_post(Header & header, Reader & cgi)
{
	try
	{
		std::string name = cgi.get_element("username");
		std::string pass = cgi.get_element("password");

		if(name.size() > 0 && pass.size() > 0)
		{
			std::string sname = std::string(name.c_str());
			std::string spass = std::string(pass.c_str());

			// If correct credentials
			if(this->auth->check_login(sname, spass) == Authenticator::LOGIN_OK)
			{
				this->auth->remove_session(sname); // Remove session from db
				std::string token = Utils::generate_token(this->TOKEN_SIZE);
				this->auth->new_session(sname, token);

                Utils::send_notify_email(sname);
                Utils::log_audit("Login.\tUser: " + sname);

				std::map<std::string, std::string> cookie_map {std::pair<std::string, std::string>("token", token + "; Max-Age=3600; HttpOnly")};
				header.redirect(Printer::Redirection_code::HOME, &cookie_map, Authenticator::VALID_SESSION);
			}
			else
			{
				header.print(this->CSS_TAG);
				std::map<std::string, std::string> special_words;

				special_words.insert(std::pair<std::string, std::string>("!?{error}", "true"));
				special_words.insert(std::pair<std::string, std::string>("!${error-string}",
										this->error_to_string(Printer::WRONG_CREDENTIALS)));

				header.Printer::special_print(special_words, this->HTML_PATH);
				std::cout << "</body></html>\n";
			}
		}
		else // Logout
		{
			std::string logout = cgi.get_element("logout");
			if(logout.size() > 0)
			{
				std::string token = this->auth->token_check(cgi);
				std::string name = this->auth->get_username(token);

				if(name.size() > 0)
					this->auth->remove_session(name);

				// No error code since it is a logout
				header.login_redirect(Authenticator::VALID_SESSION);
			}
			else
			{
				header.login_redirect(Printer::WRONG_CREDENTIALS);
			}
		}
	}
	catch (std::exception & e) 
	{
		std::cout <<  "F" << std::endl;
	}
}


std::string Login::error_to_string(int error_code)
{
	switch(error_code)
	{
		case Printer::NO_ERROR: return "";
		case Authenticator::UNEXISTENT_USER_SESSION:
		case Printer::INVALID_SESSION: return "Not logged in";
		case Printer::WRONG_CREDENTIALS: return "Wrong credentials";
		case Authenticator::UNVERIFIED: 
		case Printer::NO_VERIFIED: return "This profile is not verified yet";
		default: return "Unknown error";
	}
}