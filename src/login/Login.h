#ifndef LOGIN_H
#define LOGIN_H
#include <string>
#include <iostream>
#include <fstream>
#include <cgicc/Cgicc.h>

#include "social-network/Header.h"
#include "social-network/Authenticator.h"
#include "social-network/Reader.h"

class Login
{
private:
	const std::string CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/login/login.css\">";
	const std::string HTML_PATH = "/home/sssnadm/Desktop/social-network/pages/login/login.html";
	const unsigned int TOKEN_SIZE = 21;
	void handle_post(Header & header, Reader & cgi);
	std::string error_to_string(int error_code);
	Authenticator * auth;

public:
	Login();
	~Login();
	int run();
};



#endif