#ifndef FRIENDS_H
#define FRIENDS_H
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "social-network/Authenticator.h"
#include "social-network/Header.h"
#include "social-network/Reader.h"


class Friends
{
private:
	const std::string CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/friends/friends.css\">";
	const std::string HTML_PATH = "/home/sssnadm/Desktop/social-network/pages/friends/friends.html";
	Authenticator * auth;	
	bool logged_in = false;

	std::vector<std::map<std::string, std::string>> get_search_result(std::string & username);

public:
	Friends();
	~Friends();
	int run();

	std::vector<std::map<std::string, std::string>> get_friends(std::string & username);
	void search_user(Reader & cgi, Header & header);	
	void add_friend(Reader & cgi, Header & header, std::string & username);	
};



#endif