#include "Friends.h"

#include "social-network/Header.h"
#include "social-network/Printer.h"
#include "social-network/db_connector.h"
#include "social-network/Utils.h"

#include <cstdlib>
#include <cstring>
#include <exception>
#include <map>
#include <sstream>


Friends::Friends()
{
	this->auth = new Authenticator();
}

Friends::~Friends()
{
	delete this->auth;
}

int Friends::run()
{
	Reader cgi;
	Header header;
	std::string token = this->auth->token_check(cgi);
	std::string username = this->auth->get_username(token);
	this->logged_in = username.size() > 0 && this->auth->check_session(username, token) == 0;

	if(this->logged_in)
	{
		header.set_logged_in(true);
		header.set_name(username);
	}

	char * env = std::getenv("REQUEST_METHOD");
	if (env != 0 && strncmp(std::getenv("REQUEST_METHOD"), "POST", 4) == 0)
	{
		std::string add = cgi.get_element("add-friend");
		if(add == "Add friend")
			this->add_friend(cgi, header, username);
		else
			this->search_user(cgi, header);
	}
	else
	{
		header.print(this->CSS_TAG);
		std::map<std::string, std::string> words;


		if(this->logged_in)
		{
			std::vector< std::map<std::string, std::string> > friends = Utils::get_friends(username);

			if(friends.size() > 0)
				words.insert(std::pair<std::string, std::string>("!?{my-friends}", "true"));

			header.Printer::special_print(words, this->HTML_PATH, &friends);			
		}
		else
		{
			header.Printer::special_print(words, this->HTML_PATH);
		}

		std::cout << "</body></html>\n";
	}

	return 0;

}

void Friends::add_friend(Reader & cgi, Header & header, std::string & username)
{

	std::string user2 = cgi.get_element("uname");

	if (user2.size() > 0)
	{
		std::ostringstream query;
		query << "CALL snetwork.add_friend(\'" << username << "\',\'" << user2 << "\');";

		db_connector db;
		db.query(query.str().c_str());
	}

	header.redirect(Printer::Redirection_code::FRIENDS, NULL, Printer::NO_ERROR);
}

void Friends::search_user(Reader & cgi, Header & header)
{
	try 
	{
		std::map<std::string, std::string> search_ok;
		std::string squery = cgi.get_element("query");
		header.print(this->CSS_TAG);

		if(squery.size() > 0)
		{
			std::vector<std::map<std::string, std::string>> users =this->get_search_result(squery);

			search_ok.insert(std::pair<std::string, std::string>("!?{search}", "true"));
			search_ok.insert(std::pair<std::string, std::string>("!${search}", squery));

			if (this->logged_in) 
				search_ok.insert(std::pair<std::string, std::string>("!?{logged}", "true"));

			header.Printer::special_print(search_ok, this->HTML_PATH, &users);
			std::cout << "</body></html>\n";
		}
		else
		{
			header.Printer::special_print(search_ok, this->HTML_PATH);
			std::cout << "</body></html>\n";
		}
	}
	catch(std::exception & e)
	{
		std::cout << "F\n";
	}
}

std::vector<std::map<std::string, std::string>> Friends::get_search_result(std::string & username)
{
	std::vector<std::map<std::string, std::string>> users;

	db_connector db;

	std::ostringstream query;

	query << "CALL snetwork.search_user(\'" << username << "\');";

	MYSQL_RES * result = NULL;
	result = db.query(query.str().c_str());

	if(result != NULL)
	{
		MYSQL_ROW row;
	    while ((row = mysql_fetch_row(result)) != NULL && mysql_num_fields(result) == 2 )
	    {
			std::map<std::string, std::string> words;
			words.insert(std::pair<std::string, std::string>("!${name}", row[0]));

			if(row[1] != NULL)
			{
				words.insert(std::pair<std::string, std::string>("!?{image}", "true"));	
				words.insert(std::pair<std::string, std::string>("!${image}", row[1]));
			}

			users.push_back(words);
	    }
	}

	return users;
}