#include "social-network/Image.h"
#include "social-network/db_connector.h"

#include <sstream>
#include <fstream>
#include <cstring>
#include <string>
#include <iostream>

std::string Image::searchFormat(std::string namePhoto) 
{ 
	std::string png = ".png";
	std::string jpg = ".jpg";
	std::string jpeg = ".jpeg";
	std::string format = "";

  if (namePhoto.compare(namePhoto.size()-4,4,png) == 0)
  	format = png;
  else if (namePhoto.compare(namePhoto.size()-4,4,jpg) == 0)
  	format = jpg;
  else if (namePhoto.compare(namePhoto.size()-5,5,jpeg) == 0)
  	return jpeg;

	return format;
} 


std::string Image::save(std::string textImage, std::string name, std::string token)
{
	std::string format = "";
	format = searchFormat(name);
	std::string path = "/var/www/img/";

	std::stringstream ss;
	std::string finalName = "";
	finalName = token + format;
	ss << path << token << format;
	std::string s = ss.str();

	std::ofstream outfile (s);

	outfile << textImage << std::endl;

	outfile.close();

	return finalName;
}