#ifndef POST_H
#define POST_H
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <cgicc/Cgicc.h>

#include "social-network/Authenticator.h"
#include "social-network/Header.h"

class Post
{
private:
	const std::string CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/post/post.css\">";
	const std::string HTML_PATH = "/home/sssnadm/Desktop/social-network/pages/post/post.html";

	void handle_post(cgicc::Cgicc & cgi, std::string uname);
	void remove_post(std::string & spid, std::string & name);
	void remove_image(std::string & pid);

	Authenticator * auth;

public:
	Post();
	~Post();
	int run();
};



#endif