#include "Post.h"

#include "social-network/Printer.h"
#include "social-network/db_connector.h"
#include "social-network/Utils.h"
#include "social-network/Image.h"

#include <cstdlib>
#include <cstring>
#include <exception>
#include <map>
#include <sstream>
#include <cstdio>

Post::Post()
{
	this->auth = new Authenticator();
}

Post::~Post()
{
	delete this->auth;
}

int Post::run()
{
	Header header;
	cgicc::Cgicc cgi;
	std::string token = this->auth->token_check(cgi);
	std::string name = this->auth->get_username(token);
	long session = auth->check_session(name, token);

	if(session == Authenticator::VALID_SESSION)
	{
		char * env = std::getenv("REQUEST_METHOD");
		if (env != 0 && strncmp(std::getenv("REQUEST_METHOD"), "POST", 4) == 0)
		{
			cgicc::form_iterator pid = cgi.getElement("delete");

			if(pid != cgi.getElements().end())
			{
				std::string spid = std::string((**pid).c_str());
				this->remove_post(spid, name);
			}
			else
				this->handle_post(cgi, name);

			header.redirect(Printer::Redirection_code::HOME, NULL, Printer::NO_ERROR);
		}
		else
		{
			header.set_logged_in(true);
			header.set_name(name);

			std::map<std::string, std::string> words;
			
			std::vector< std::map<std::string, std::string> > friends = Utils::get_friends(name);
	
			if(friends.size() > 0)
				words.insert(std::pair<std::string, std::string>("!?{friends}", "true"));
			else
				words.insert(std::pair<std::string, std::string>("!?{no-friends}", "true"));

			header.print(this->CSS_TAG);
			header.Printer::special_print(words, this->HTML_PATH, &friends);
			std::cout << "</body></html>\n";
		}
	}
	else
	{
		header.login_redirect(Printer::INVALID_SESSION);
	}

	return 0;

}

void Post::handle_post(cgicc::Cgicc & cgi, std::string uname)
{
	cgicc::form_iterator name = cgi.getElement("username");
	cgicc::form_iterator mess = cgi.getElement("message");
	cgicc::const_form_iterator end = cgi.getElements().end();

	auto file = cgi.getFiles();
	std::string path = "";
	
	if(name != end && mess != end )
	{
		std::string sname = std::string((**name).c_str());
		std::string smess = std::string((**mess).c_str());

		if(file.size() > 0)
		path = Image::save((file.at(0)).getData(), file.at(0).getFilename(), Utils::generate_token(50)); 

		db_connector db;
		std::string token = this->auth->token_check(cgi);

		std::ostringstream query;
		std::string real_path = path.size() > 0 ? ("\'" + path + "\'") : "null";;

		query << "CALL snetwork.add_post(\'" << this->auth->get_username(token) 
				<< "\',\'" << sname << "\',\'" << smess << "\'," << real_path
				<< ",\'" << Utils::generate_token(45) << "\');";

		db.query(query.str().c_str());
        Utils::log_audit("Post message.\tUser: " + uname + " to: " + sname);

	}
}

void Post::remove_post(std::string & spid, std::string & name)
{
	Utils::log(spid + " / " + name);

	if(spid.size() == 45)
	{
		this->remove_image(spid);

		db_connector db;
		std::ostringstream query;
		query << "CALL snetwork.remove_post(\'" << spid << "\',\'" << name << "\');";
		db.query(query.str().c_str());
        Utils::log_audit("Remove message.\tUser: " + name);
	}
}

void Post::remove_image(std::string & pid)
{
	db_connector db;
	std::ostringstream image;
	image << "CALL snetwork.get_post_img(\'" << pid << "\');";
	MYSQL_RES * img_name = db.query(image.str().c_str());

	if(img_name != NULL)
	{
		MYSQL_ROW row = mysql_fetch_row(img_name);
		if(row[0] != NULL)
		{
			std::ostringstream file;
			file << "/var/www/img/" << std::string(row[0]);
			if(remove(file.str().c_str()) != 0)
			{
				Utils::log(std::strerror(errno));
			}

		}
	}
}