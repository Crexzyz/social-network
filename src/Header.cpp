#include "social-network/Header.h"

#include <iostream>

Header::Header()
{

}

Header::~Header()
{

}

void Header::set_logged_in(bool logged_in)
{
	this->logged_in = logged_in;
}

void Header::set_name(std::string name)
{
	this->name = name;
}

void Header::print(const std::string& extra_css)
{	
	this->Printer::basic_print();
	
	std::cout << "<html><head><title>Super secure social network</title>\n";

	std::cout << this->CSS_TAG << std::endl;
	std::cout << this->SHARED_CSS_TAG << std::endl;

	if(extra_css.size() > 0)
		std::cout << extra_css << std::endl;

	std::cout << "</head><body>\n";

	std::map<std::string, std::string> words;

	if (!this->logged_in)
		words.insert(std::pair<std::string, std::string>("!${user-name}", "Not logged in"));
	else
		words.insert(std::pair<std::string, std::string>("!${user-name}", this->name));

	this->Printer::special_print(words, this->HTML_PATH);
}