CC = g++
CFLAGS = -Iinclude -I/usr/include/mysql
CFLAGS += -Wall -Wextra
LDFLAGS = `mysql_config --libs` -lcgicc

EXECUTABLES = login home register friends post profile validate
SOURCE_DIR = src
BUILD_DIR = build

# Comment pending xd
EXEC_PATHS := $(patsubst %, $(SOURCE_DIR)/%, $(EXECUTABLES))
# Get all files located in src/ 
SRCS := $(wildcard $(SOURCE_DIR)/*.cpp)
# Generate .o file names for each common .c file
OBJS := $(patsubst $(SOURCE_DIR)/%.cpp, $(BUILD_DIR)/%.o, $(SRCS))

# Link/Compile each .o and generate executables
all: $(BUILD_DIR) $(OBJS) execs
	@echo "\e[38;5;82mCompiled.\e[m"

# Compile each executable using its Makefile
execs:
	@for dir in $(EXEC_PATHS); do \
		$(MAKE) -C $$dir; \
	done

install: all css-install
	cp $(patsubst %, ./build/%, $(EXECUTABLES)) /var/www/cgi-bin/

css-install:
	cp pages /var/www/html -r

# Compile each .c file in source folder
$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.cpp
	$(CC) $(CFLAGS) $(LDFLAGS) -c $^ -o $@ 

# Delete all object files and the executable
.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)

# Creates folder for object files
$(BUILD_DIR):
	mkdir -p $@

