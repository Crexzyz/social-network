#ifndef DB_CONNECTOR_H
#define DB_CONNECTOR_H
#include <mysql.h>
#include <string>

class db_connector
{

private:
	const char* host = "localhost";
	const char* db_name = "snetwork";
	const char* user = "snetwork";
	const char* password = ".snetwork-adm&";
	const char* socket = NULL;
	const int port = 3306;
	const int opt = 0;

   	MYSQL * connection;
    MYSQL_RES * query_result;

    bool last_query_freed = true;

    void free_result();
    bool connect();


public:
	db_connector();
	~db_connector();


	MYSQL_RES * query(const char * query);
	void clean_string(std::string & subquery);
	void register_insert(std::string & username, std::string & email, std::string & password);

	void search_users(std::string & username);

	
};

#endif