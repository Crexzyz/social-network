#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <map>
#include <string>
#include <ctime>
#include <chrono>

#include "social-network/db_connector.h"

class Utils
{
private:


public:
    Utils();
    ~Utils();

    static std::vector<std::map<std::string, std::string>> get_friends(std::string & username);
    static std::string generate_token(unsigned int size);
    static void log(std::string text);
    static void log_audit(std::string text);
    static void raw_print();
    static char decode_html(std::string & text);
    static void send_mail(std::string & username, std::string & mail);
    static void resend_mail(std::string & username, std::string & mail, std::string token);
    static void send_notify_email(std::string & username);
};

#endif
