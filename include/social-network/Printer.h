#ifndef PRINTER_H
#define PRINTER_H
#include <fstream>
#include <map>
#include <string>
#include <vector>

class Printer
{

private:
	const std::string REDIRECT_HTML = "/home/sssnadm/Desktop/social-network/pages/redirect.html";
	const std::string ERROR_HTML = "/home/sssnadm/Desktop/social-network/pages/error.html";

protected:
	void set_cookie(std::string key, std::string value);
	void cookie_header(std::map<std::string, std::string> & cookies);
	void replace_special(std::string & line, const std::map<std::string, std::string> & special_words);
	bool check_conditional_print(std::string variable,
								const std::map<std::string, std::string> & special_words,
								const std::map<std::string, std::string> & alternate_words);

public:
	enum Login_codes {
		NO_ERROR = 0,
		INVALID_SESSION = 1,
		WRONG_CREDENTIALS = 2,
		NO_VERIFIED = 101,
		VERIFIED = 100
	};

	enum Redirection_code
	{
		LOGIN,
		HOME,
		FRIENDS,
		POST,
		REGISTER
	};

	Printer();
	~Printer();


	void print(const std::string & path);
	void basic_print();
	void special_print(const std::map<std::string, std::string> & special_words,
						const std::string & special_path,
						const std::vector< std::map<std::string, std::string> > * posts);

	void special_print(const std::map<std::string, std::string> & special_words,
						const std::string & special_path);

	void loop_print(const std::map<std::string, std::string> & special_words,
					std::ifstream & opened_file,
					const std::vector<std::map<std::string, std::string>> & posts);

	void conditional_print(const std::map<std::string, std::string> & variables,
						std::vector<std::string>::iterator & line_iterator,
						const std::map<std::string, std::string> & special_words);

	void conditional_print(std::string & cond_variable, std::ifstream & opened_file,
						const std::map<std::string, std::string> & special_words,
						const std::vector< std::map<std::string, std::string> > * posts);

	void redirect(int redirection_code, std::map<std::string, std::string> * cookies, int error_code);
	void login_redirect(int error_code);
};


#endif