#ifndef READER_H
#define READER_H

#include <map>
#include <string>

class Reader
{
private:
    enum Multipart_states {
        NEW_ELEMENT,
        READING_ELEMENT,
        VALUE_READ_NEWLINE,
        IMAGE_READ_NEWLINE,
        READING_VALUE,
        READING_IMAGE
    };

    enum Plain_states {
        READING_NAME,
        READING_VALUE_P
    };

    std::map<std::string, std::string> elements;
    std::map<std::string, std::string> cookies;
    
    std::string file_name;
    std::string file_data;

    void read_elements();

    void read_multipart(const std::string & separator);
    std::string find_element_name(std::string & line, std::string finder);

    void read_plain(std::string input, std::map<std::string, std::string> & target);

    void read_cookies();

public:
    Reader();
    ~Reader();

    std::string get_element(std::string name);
    std::string get_cookie(const std::string name);
    std::string get_filename();
    std::string get_data();
};

#endif