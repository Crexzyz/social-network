#ifndef HEADER_H
#define HEADER_H
#include <string>
#include <map>

#include "social-network/Printer.h"

class Header : public Printer
{
private:
	const std::string SHARED_CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/shared.css\">";
	const std::string CSS_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../pages/header/header.css\">";
	const std::string HTML_PATH = "/home/sssnadm/Desktop/social-network/pages/header/header.html";
	bool logged_in = false;
	std::string name = "";

public:
	Header();
	~Header();

	void set_logged_in(bool logged_in);
	void set_name(std::string name);
	
	
	void print(const std::string & file);

};


#endif