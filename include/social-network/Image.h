#ifndef IMAGE_H
#define IMAGE_H

#include <vector>
#include <map>
#include <string>

#include "social-network/db_connector.h"

class Image
{
private:


public:
    Image();
    ~Image();

    static std::string save(std::string textImage, std::string name, std::string token);
	static std::string searchFormat(std::string s1);
};

#endif
