#ifndef AUTHENTICATOR_H
#define AUTHENTICATOR_H

#include <string>
#include <cgicc/Cgicc.h>

#include "social-network/Reader.h"

class Authenticator
{
private:

public:
	enum Session_code {
		VALID_SESSION = 0,
		EXPIRED_SESSION = 1,
		UNEXISTENT_TOKEN = 2,
		UNEXISTENT_USER_SESSION = 3
	};

	enum Login_code {
		LOGIN_OK = 0,
		BAD_PASSWORD = 1,
		BAD_USER = 2,
		UNVERIFIED = 5
	};

	enum Error_code {
		EMPTY_RESULT = 9,
		QUERY_ERROR = 10
	};

	Authenticator();
	~Authenticator();

	std::string token_check(cgicc::Cgicc & cgi);
	std::string token_check(Reader & reader);
	
	void new_session(std::string username, std::string token);
	void remove_session(std::string username);
	std::string get_username(std::string token);
	long check_session(std::string username, std::string token);
	long check_login(std::string username, std::string password);
};


#endif